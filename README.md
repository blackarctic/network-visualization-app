# Vis Network App

Used for the visualization of graph data.

Specifically, I am using this to analyze import dependencies.

## Getting Started

Open `public/lib/main` and modify the `rawNodes` and `rawEdges` to fit your use case.

Then, start up the server to view the network visualization app.

```
http-server public
```
