// Data Definitions...

var rawNodes = [
  { id: "root" },

  // operations
  { id: "operations/index.public.ts" },
  { id: "operations/index.ts" },
  { id: "operations/operations.globalStatus.ts" },
  { id: "operations/operations.invalidate.ts" },
  { id: "operations/operations.mutations.ts" },
  { id: "operations/operations.query.helpers.ts" },
  { id: "operations/operations.query.ts" },
  { id: "operations/operations.ts" },

  // currentOperation
  { id: "currentOperation/currentOperation.globalStatus.ts" },
  { id: "currentOperation/currentOperation.invalidate.ts" },
  { id: "currentOperation/currentOperation.ts" },
  { id: "currentOperation/index.public.ts" },
  { id: "currentOperation/index.ts" },

  // currentOperationId
  { id: "currentOperationId/currentOperationId.ts" },
  { id: "currentOperationId/index.public.ts" },
  { id: "currentOperationId/index.ts" },

  // operation
  { id: "operation/index.ts" },
  { id: "operation/operation.invalidate.ts" },
  { id: "operation/operation.query.ts" },
  { id: "operation/operation.ts" },

  // preOperations
  { id: "preOperations/index.ts" },
  { id: "preOperations/preOperations.invalidate.ts" },
  { id: "preOperations/preOperations.query.helpers.ts" },
  { id: "preOperations/preOperations.query.ts" },
  { id: "preOperations/preOperations.ts" },
];

var createEdges = ({ from, to }) => {
  return from.map((x) => ({ from: x, to }));
};

var rawEdges = [
  //////////
  // root //
  //////////

  ...createEdges({
    from: ["operations/index.public.ts", "operations/index.ts"],
    to: "root",
  }),

  ////////////////
  // operations //
  ////////////////

  ...createEdges({
    from: [
      "currentOperation/index.public.ts",
      "currentOperationId/index.public.ts",
      "operations/operations.ts",
    ],
    to: "operations/index.public.ts",
  }),
  ...createEdges({
    from: [
      "currentOperation/index.ts",
      "currentOperationId/index.ts",
      "operations/operations.ts",
    ],
    to: "operations/index.ts",
  }),
  ...createEdges({
    from: [],
    to: "operations/operations.globalStatus.ts",
  }),
  ...createEdges({
    from: ["operation/index.ts"],
    to: "operations/operations.invalidate.ts",
  }),
  ...createEdges({
    from: [
      "currentOperationId/index.ts",
      "currentOperation/index.ts",
      "currentOperation/currentOperation.globalStatus.ts",
      "operations/operations.invalidate.ts",
      "operations/operations.globalStatus.ts",
    ],
    to: "operations/operations.mutations.ts",
  }),
  ...createEdges({
    from: ["preOperations/preOperations.query.ts"],
    to: "operations/operations.query.helpers.ts",
  }),
  ...createEdges({
    from: [
      "preOperations/preOperations.query.ts",
      "operation/operation.query.ts",
      "currentOperationId/index.ts",
      "operations/operations.query.helpers.ts",
      "operations/operations.globalStatus.ts",
    ],
    to: "operations/operations.query.ts",
  }),
  ...createEdges({
    from: [
      "preOperations/index.ts",
      "operations/operations.query.ts",
      "operations/operations.globalStatus.ts",
      "operations/operations.invalidate.ts",
      "operations/operations.mutations.ts",
    ],
    to: "operations/operations.ts",
  }),

  //////////////////////
  // currentOperation //
  //////////////////////

  ...createEdges({
    from: [],
    to: "currentOperation/currentOperation.globalStatus.ts",
  }),
  ...createEdges({
    from: ["currentOperationId/index.ts", "operation/index.ts"],
    to: "currentOperation/currentOperation.invalidate.ts",
  }),
  ...createEdges({
    from: [
      "currentOperationId/index.ts",
      "operation/index.ts",
      "currentOperation/currentOperation.invalidate.ts",
      "currentOperation/currentOperation.globalStatus.ts",
    ],
    to: "currentOperation/currentOperation.ts",
  }),
  ...createEdges({
    from: ["currentOperation/currentOperation.ts"],
    to: "currentOperation/index.public.ts",
  }),
  ...createEdges({
    from: ["currentOperation/currentOperation.ts"],
    to: "currentOperation/index.ts",
  }),

  ////////////////////////
  // currentOperationId //
  ////////////////////////

  ...createEdges({
    from: ["preOperations/index.ts"],
    to: "currentOperationId/currentOperationId.ts",
  }),
  ...createEdges({
    from: ["currentOperationId/currentOperationId.ts"],
    to: "currentOperationId/index.public.ts",
  }),
  ...createEdges({
    from: ["currentOperationId/currentOperationId.ts"],
    to: "currentOperationId/index.ts",
  }),

  ///////////////
  // operation //
  ///////////////

  ...createEdges({
    from: ["operation/operation.ts"],
    to: "operation/index.ts",
  }),
  ...createEdges({
    from: ["operation/operation.query.ts"],
    to: "operation/operation.invalidate.ts",
  }),
  ...createEdges({
    from: [
      "currentOperationId/index.ts",
      "currentOperation/currentOperation.globalStatus.ts",
    ],
    to: "operation/operation.query.ts",
  }),
  ...createEdges({
    from: ["operation/operation.query.ts", "operation/operation.invalidate.ts"],
    to: "operation/operation.ts",
  }),

  ///////////////////
  // preOperations //
  ///////////////////

  ...createEdges({
    from: ["preOperations/preOperations.ts"],
    to: "preOperations/index.ts",
  }),
  ...createEdges({
    from: ["preOperations/preOperations.query.ts"],
    to: "preOperations/preOperations.invalidate.ts",
  }),
  ...createEdges({
    from: [],
    to: "preOperations/preOperations.query.helpers.ts",
  }),
  ...createEdges({
    from: ["preOperations/preOperations.query.helpers.ts"],
    to: "preOperations/preOperations.query.ts",
  }),
  ...createEdges({
    from: [
      "preOperations/preOperations.query.ts",
      "preOperations/preOperations.invalidate.ts",
    ],
    to: "preOperations/preOperations.ts",
  }),

  //////////////
  // TEMPLATE //
  //////////////

  ...createEdges({
    from: [],
    to: "",
  }),
];

// Checks...

rawEdges.forEach((edge) => {
  const nodeIds = rawNodes.map((x) => x.id);
  if (!nodeIds.includes(edge.from)) {
    throw new Error(
      `"${edge.from}" cannot be specified in an edge because it is not listed as a node id`
    );
  }
  if (!nodeIds.includes(edge.to)) {
    throw new Error(
      `"${edge.to}" cannot be specified in an edge because it is not listed as a node id`
    );
  }
});

// Cycle Detection...

var detectedCycle = null;
(() => {
  var edgesForCycleDetect = {};
  rawEdges.forEach((edge) => {
    var existing = edgesForCycleDetect[edge.from];
    edgesForCycleDetect[edge.from] = existing
      ? _.uniq([...existing, edge.to])
      : [edge.to];
  });

  var nodesForCycleDetect = rawNodes.map((x) => x.id);

  var startNodes = nodesForCycleDetect;
  var getConnectedNodes = (node) => edgesForCycleDetect[node];

  detectedCycle = findDirectedCycle(startNodes, getConnectedNodes);
  console.log("detectedCycle", detectedCycle);
})();

// Network Visualization...

(() => {
  var nodes = new vis.DataSet(
    rawNodes.map((x) => ({
      ...x,
      label: x.id,
      color: detectedCycle && detectedCycle.includes(x.id) ? "red" : undefined,
    }))
  );
  var edges = new vis.DataSet(rawEdges);

  var container = document.getElementById("my-network");
  var data = {
    nodes: nodes,
    edges: edges,
  };
  var options = {
    edges: {
      arrows: "to",
      color: { color: "#AAAAAA", highlight: "#08B922" },
    },
    physics: {
      enabled: false,
    },
    layout: {
      hierarchical: {
        enabled: true,
        levelSeparation: 150,
        nodeSpacing: 300,
        treeSpacing: 150,
        blockShifting: true,
        edgeMinimization: true,
        parentCentralization: true,
        direction: "DU", // UD, DU, LR, RL
        sortMethod: "directed", // hubsize, directed
        shakeTowards: "roots", // roots, leaves
      },
    },
  };
  var network = new vis.Network(container, data, options);
  window.visNetwork = network;
})();
